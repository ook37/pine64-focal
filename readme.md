**PinePhone (non-Pro) users:** make sure you have followed the first section of steps at https://ubports.com/blog/ubports-news-1/post/pinephone-and-pinephone-pro-3889 before installing!

**Strongly Recommended Modem Firmware:**
- [Biktorgj's Open Modem Firmware v0.7.2](https://github.com/the-modem-distro/pinephone_modem_sdk/releases/tag/0.7.2)
  - ADSP Version 30.006.30.006

### PinePhone (Pro) + PineTab/2: Ubuntu Touch 20.04 Port
_features with a star are for phones only_

| Currently supported features: | Features partially working: | Features currently not working: |
|-------------------------------|-----------------------------|---------------------------------|
|  Boot into full UI            |  SSH                        |  Cameras (front and back)       |
|  Touchscreen                  |  Wired external monitor     |  Proximity sensor               |
|  On-Screen Keyboard           |  GPS                        |
|  SD card detection/access     |                             |
|  Online charging              |                             |
|  RTC time                     |                             |  
|  Shutdown/reboot              |                             |  
|  Wifi                         |                             
|  Bluetooth (FTP, Audio) _(PineTab2 needs port)_      |
|  Flight Mode                  |
|  Apparmor patches             |
|  PinePhone Keyboard*          |
|  Video playback               | 
|  Manual brightness            |
|  Notification LED*            |
|  Waydroid (mainline)          |
|  UART/serial console          |
|  Modem (Internet, SMS)*       |
|  On-device audio              |
|  Charging indicator LED*      |
|  Ethernet                     |
|  Flashlight/torch             |
|  Accelerometer/gyroscope      |

### Debos build recipes

This uses debos to build (sdcard) images for devices

Supported devices:
 - Pine64
    - PinePhone Don't be evil development kit
    - PinePhone Developer Batch (1.0)
    - PinePhone Braveheart (1.1)
    - PinePhone (1.2)
    - PinePhone Pro
    - PineTab
    - PineTab2

**NOTE:** Tow-Boot is ***required*** for PinePhone + Pro!
